# Awale
Oware(Awalé) is an abstract strategy game among the mancala family of board games played worldwide with slight variations as to the layout of the game, number of players and strategy of play.
This project is written entirely with HTML and JavaScript.<br>
<img src="img/awale1.png" width="150" style="margin:10px" >
<img src="img/awale2.png" width="150" style="margin:10px">
<img src="img/awale3.png" width="150" style="margin:10px">


## Install
1. Install the dependencies in the local node_modules folder.
```
npm install
```
2. Run the build field from the package.json scripts field
```
npm run build
```
3. Open index.html in a web browser
